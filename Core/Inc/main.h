/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SW1_Pin GPIO_PIN_0
#define SW1_GPIO_Port GPIOC
#define SW2_Pin GPIO_PIN_1
#define SW2_GPIO_Port GPIOC
#define SW0_Pin GPIO_PIN_2
#define SW0_GPIO_Port GPIOC
#define DC_Pin GPIO_PIN_1
#define DC_GPIO_Port GPIOB
#define CS0_Pin GPIO_PIN_2
#define CS0_GPIO_Port GPIOB
#define LED_1_Pin GPIO_PIN_12
#define LED_1_GPIO_Port GPIOB
#define CS1_Pin GPIO_PIN_6
#define CS1_GPIO_Port GPIOC
#define CS2_Pin GPIO_PIN_7
#define CS2_GPIO_Port GPIOC
#define CS3_Pin GPIO_PIN_8
#define CS3_GPIO_Port GPIOC
#define CS4_Pin GPIO_PIN_9
#define CS4_GPIO_Port GPIOC
#define LED_2_Pin GPIO_PIN_8
#define LED_2_GPIO_Port GPIOA
#define CS5_Pin GPIO_PIN_9
#define CS5_GPIO_Port GPIOA
#define CS6_Pin GPIO_PIN_10
#define CS6_GPIO_Port GPIOA
#define CS7_Pin GPIO_PIN_10
#define CS7_GPIO_Port GPIOC
#define CS8_Pin GPIO_PIN_11
#define CS8_GPIO_Port GPIOC
#define CS9_Pin GPIO_PIN_12
#define CS9_GPIO_Port GPIOC
#define RES_Pin GPIO_PIN_5
#define RES_GPIO_Port GPIOB
#define DISPLAY_Pin GPIO_PIN_6
#define DISPLAY_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

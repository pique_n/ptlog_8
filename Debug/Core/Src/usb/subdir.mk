################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/usb/usbd_core.c \
../Core/Src/usb/usbd_stm32l100_devfs.c 

S_UPPER_SRCS += \
../Core/Src/usb/usbd_stm32l100_devfs_asm.S 

OBJS += \
./Core/Src/usb/usbd_core.o \
./Core/Src/usb/usbd_stm32l100_devfs.o \
./Core/Src/usb/usbd_stm32l100_devfs_asm.o 

S_UPPER_DEPS += \
./Core/Src/usb/usbd_stm32l100_devfs_asm.d 

C_DEPS += \
./Core/Src/usb/usbd_core.d \
./Core/Src/usb/usbd_stm32l100_devfs.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/usb/%.o Core/Src/usb/%.su: ../Core/Src/usb/%.c Core/Src/usb/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g -DDEBUG -DUSE_HAL_DRIVER -DSTM32L152xBA -DSTM32L1 -c -I../Core/Inc -I../Drivers/STM32L1xx_HAL_Driver/Inc -I../Drivers/STM32L1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32L1xx/Include -I../Drivers/CMSIS/Include -I../Drivers/ptlog_8 -I../Core/Inc/usb -Og -ffunction-sections -fdata-sections -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/usb/%.o: ../Core/Src/usb/%.S Core/Src/usb/subdir.mk
	arm-none-eabi-gcc -mcpu=cortex-m3 -g3 -DDEBUG -c -I"C:/Users/pique_n/Documents/GitHub/ptlog_8/Core/Inc/usb" -x assembler-with-cpp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@" "$<"

clean: clean-Core-2f-Src-2f-usb

clean-Core-2f-Src-2f-usb:
	-$(RM) ./Core/Src/usb/usbd_core.d ./Core/Src/usb/usbd_core.o ./Core/Src/usb/usbd_core.su ./Core/Src/usb/usbd_stm32l100_devfs.d ./Core/Src/usb/usbd_stm32l100_devfs.o ./Core/Src/usb/usbd_stm32l100_devfs.su ./Core/Src/usb/usbd_stm32l100_devfs_asm.d ./Core/Src/usb/usbd_stm32l100_devfs_asm.o

.PHONY: clean-Core-2f-Src-2f-usb


################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/ptlog_8/DISD_DisplayDriver/DISD_DisplayDriver.c \
../Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x7.c \
../Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x8.c \
../Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_6x8.c \
../Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_8x8.c 

OBJS += \
./Drivers/ptlog_8/DISD_DisplayDriver/DISD_DisplayDriver.o \
./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x7.o \
./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x8.o \
./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_6x8.o \
./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_8x8.o 

C_DEPS += \
./Drivers/ptlog_8/DISD_DisplayDriver/DISD_DisplayDriver.d \
./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x7.d \
./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x8.d \
./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_6x8.d \
./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_8x8.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/ptlog_8/DISD_DisplayDriver/%.o Drivers/ptlog_8/DISD_DisplayDriver/%.su: ../Drivers/ptlog_8/DISD_DisplayDriver/%.c Drivers/ptlog_8/DISD_DisplayDriver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g -DDEBUG -DUSE_HAL_DRIVER -DSTM32L152xBA -DSTM32L1 -c -I../Core/Inc -I../Drivers/STM32L1xx_HAL_Driver/Inc -I../Drivers/STM32L1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32L1xx/Include -I../Drivers/CMSIS/Include -I../Drivers/ptlog_8 -I../Core/Inc/usb -Og -ffunction-sections -fdata-sections -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-ptlog_8-2f-DISD_DisplayDriver

clean-Drivers-2f-ptlog_8-2f-DISD_DisplayDriver:
	-$(RM) ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_DisplayDriver.d ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_DisplayDriver.o ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_DisplayDriver.su ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x7.d ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x7.o ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x7.su ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x8.d ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x8.o ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_5x8.su ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_6x8.d ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_6x8.o ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_6x8.su ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_8x8.d ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_8x8.o ./Drivers/ptlog_8/DISD_DisplayDriver/DISD_Font_8x8.su

.PHONY: clean-Drivers-2f-ptlog_8-2f-DISD_DisplayDriver


################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/ptlog_8/UTIL_Utility/UTIL_Utility.c 

OBJS += \
./Drivers/ptlog_8/UTIL_Utility/UTIL_Utility.o 

C_DEPS += \
./Drivers/ptlog_8/UTIL_Utility/UTIL_Utility.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/ptlog_8/UTIL_Utility/%.o Drivers/ptlog_8/UTIL_Utility/%.su: ../Drivers/ptlog_8/UTIL_Utility/%.c Drivers/ptlog_8/UTIL_Utility/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g -DDEBUG -DUSE_HAL_DRIVER -DSTM32L152xBA -DSTM32L1 -c -I../Core/Inc -I../Drivers/STM32L1xx_HAL_Driver/Inc -I../Drivers/STM32L1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32L1xx/Include -I../Drivers/CMSIS/Include -I../Drivers/ptlog_8 -I../Core/Inc/usb -Og -ffunction-sections -fdata-sections -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-ptlog_8-2f-UTIL_Utility

clean-Drivers-2f-ptlog_8-2f-UTIL_Utility:
	-$(RM) ./Drivers/ptlog_8/UTIL_Utility/UTIL_Utility.d ./Drivers/ptlog_8/UTIL_Utility/UTIL_Utility.o ./Drivers/ptlog_8/UTIL_Utility/UTIL_Utility.su

.PHONY: clean-Drivers-2f-ptlog_8-2f-UTIL_Utility


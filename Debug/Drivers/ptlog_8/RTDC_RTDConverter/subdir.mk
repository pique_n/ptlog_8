################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/ptlog_8/RTDC_RTDConverter/RTDC_RTDConverter.c 

OBJS += \
./Drivers/ptlog_8/RTDC_RTDConverter/RTDC_RTDConverter.o 

C_DEPS += \
./Drivers/ptlog_8/RTDC_RTDConverter/RTDC_RTDConverter.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/ptlog_8/RTDC_RTDConverter/%.o Drivers/ptlog_8/RTDC_RTDConverter/%.su: ../Drivers/ptlog_8/RTDC_RTDConverter/%.c Drivers/ptlog_8/RTDC_RTDConverter/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g -DDEBUG -DUSE_HAL_DRIVER -DSTM32L152xBA -DSTM32L1 -c -I../Core/Inc -I../Drivers/STM32L1xx_HAL_Driver/Inc -I../Drivers/STM32L1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32L1xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc/usb -I../Drivers/ptlog_8 -Og -ffunction-sections -fdata-sections -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-ptlog_8-2f-RTDC_RTDConverter

clean-Drivers-2f-ptlog_8-2f-RTDC_RTDConverter:
	-$(RM) ./Drivers/ptlog_8/RTDC_RTDConverter/RTDC_RTDConverter.d ./Drivers/ptlog_8/RTDC_RTDConverter/RTDC_RTDConverter.o ./Drivers/ptlog_8/RTDC_RTDConverter/RTDC_RTDConverter.su

.PHONY: clean-Drivers-2f-ptlog_8-2f-RTDC_RTDConverter


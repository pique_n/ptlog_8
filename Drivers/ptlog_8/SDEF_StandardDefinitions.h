//=================================================================================================
//
// Company:          Paul Scherrer Institut
//                   5232 Villigen PSI
//                   Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       PtLog_8
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:           Standard definitions (data types, structures etc.)
// Filename:         SDEF_StandardDefinitions.h
// Date:             Handled by Subversion (version control system)
// Revision:         Handled by Subversion (version control system)
// History:          Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------
//
// Description:      This header file contains the data type definitions. These definitions must be 
//                   used by all modules. Furthermore all global definitions, structures,  
//                   enumerations, etc. are collected inside this file
//
//=================================================================================================

#ifndef STANDARD_DEFINITIONS_H_INCLUDED
#define STANDARD_DEFINITIONS_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif



//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================

#define SDEF_S32MAX   ((S32)(2147483647))     // Max S32 value
#define SDEF_S32MIN   ((S32)(-2147483647 - 1) // Min S32 value
#define SDEF_U32MAX   ((U32)(0xFFFFFFFF))     // Max U32 value
#define SDEF_S16MAX   ((S16)(32767))          // Max U16 value
#define SDEF_S16MIN   ((S16)(-32767 - 1))     // Min S16 value
#define SDEF_U16MAX   ((U16)(0xFFFF))         // Max S16 value



//=================================================================================================
// DATA TYPES:       Definition of standard data types to be used by all modules. 
// 
// Prefixes:         Each data type has its own prefix (see data type definitions below).
//                   Other definitions are:
//             XXXX_ Module identifer and file name. Example: "RTCK_RealtimeClock.c". The
//                   identifer is also used for global (public) definitions inside the module.
//             g_    Global scope (variables). Example: "g_u32ElapsedTime".
//             m_    Module scope (variables). Example: "m_u32ElapsedTime".
//             a     Array.
//             p     Pointer.
//             pfn   Pointer to function.
//             St    Definition of a structure.
//             st    Instance of a structure. Example:
//                   typedef struct
//                   {
//                      S32 s32CurrentMainsL1;
//                      S32 s32CurrentMainsL2;
//                      S32 s32CurrentMainsL3;
//                      } StMainsCurrent;
//                   StMainsCurrent m_stMainsCurrent;
//             En    Definition of an enumneration.
//             e     Member of an enumeration.
//             en    Instance of an enumeration. Example:
//                   typedef enum
//                   {
//                      eDeviceIdle = 0,
//                      eDeviceActive,
//                   } EnDeviceState;
//                   EnDeviceState m_enDeviceState = eDeviceIdle;
//
//                   Example: module scope, array of enums:
//                   m_aenInputs[] = [ eInput1, eInput2, eInput3];
//=================================================================================================

// Allow direct access to variables and functions while unit testing using GCC. This makes 
// writing the test script much easier. The definition of the datatypes makes the code protable.

#define  LOCAL                static            // m_    Local data type and visible within the 
                                                //       actual file only
#define  PRIVATE              static            //       Private function and visible within the 
                                                //       actual file only
#define  INLINE               __inline          //       force the compiler to inline the function  
#define  EXTERN               extern            //       Defined at external level
#define  CONST                const             //       Constant data definition
#define  VOLATILE             volatile          //       Not to be placed in register
#define  VOID                 void              // v     Void type
typedef  VOID*                PVOID;            // pv    Pointer to void type
typedef  char                 CHAR;             // c     Character
typedef  CHAR*                PCHAR;            // pc    Pointer to character
typedef  CHAR*                PSZ;              // psz   Pointer to zero terminated string
typedef  CONST CHAR*          PSZC;             // psz   Pointer to constant zero terminated string
typedef  unsigned char        U8;               // u8    Unsigned byte (DSP is 16 bit anyway)
typedef  U8*                  PU8;              // pu8   Pointer to unsigned byte
typedef  signed char          S8;               // s8    Signed byte (DSP is 16 bit anyway)
typedef  S8*                  PS8;              // ps8   Pointer to signed byte
typedef  unsigned short int   U16;              // u16   Unsigned short integer
typedef  U16*                 PU16;             // pu16  Pointer to unsigned short integer
typedef  signed short int     S16;              // s16   Signed short integer
typedef  S16*                 PS16;             // ps16  Pointer to signed short integer
typedef  long unsigned int    U32;              // u32   Unsigned integer
typedef  U32*                 PU32;             // pu32  Pointer to unsigned int integer
typedef  long signed int      S32;              // s32   Signed integer
typedef  S32*                 PS32;             // ps32  Pointer to signed integer
typedef  unsigned long long   U64;              // u64   Unsigned long long integer
typedef  U64*                 PU64;             // pu64  Pointer to unsigned long long integer
typedef  signed long long     S64;              // s64   Signed long long integer
typedef  S64*                 PS64;             // ps64  Pointer to signed long long integer
typedef  float                FLOAT;            // fl    Float data type (32 bit)
typedef  FLOAT*               PFLOAT;           // pfl   Pointer to float
typedef  double               DOUBLE;           // db    Double data type (64 bit)
typedef  DOUBLE*              PDOUBLE;          // pdb   Pointer to double
typedef  U32                  BOOL;             // bo    Boolean
typedef  BOOL*                PBOOL;            // pbo   Pointer to bool data type

#ifndef FALSE
   #define FALSE              0                 //       False --> must be zero,
#endif

#ifndef TRUE
   #define TRUE               1                 //       True --> must be one
#endif

#ifndef NULL
   #define NULL               ((VOID *) 0)
#endif

#define __NORETURN            __attribute__((noreturn)) // funciton attribute, function does not return

typedef enum
{
  SDEF_eNormalByteOrder        = 0,
  SDEF_eReverseByteOrder       = 1,  
} SDEF_EnByteOrder;


//=================================================================================================
// Section:       MACROS
// Description:   Definition of global macros (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of global enumerations (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of global Structures (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL VARIABLES
// Description:   Definition of global variables (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL CONSTANTS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================



//=================================================================================================
// Section:       GLOBAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of global functions (visible by all modules).
//=================================================================================================

#ifdef __cplusplus
}
#endif

#endif

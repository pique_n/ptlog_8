//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       PtLog_8
// Author:        Noah Piqué (noah.pique@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        RTDC_RTDConverter
// Filename:      RTDC_RTDConverter.c
// Date:          Handled by Subversion (version control system)
// Revision:      Handled by Subversion (version control system)
// History:       Handled by Subversion (version control system)
//
//-------------------------------------------------------------------------------------------------
//
// Description:   RTD to SPI Converter
//
//=================================================================================================



//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files.
//=================================================================================================


#include "stm32l1xx_hal.h"
#include "RTDC_RTDConverter.h"
#include "UTIL_Utility/UTIL_Utility.h"
#include "main.h"


//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

// define commands
#define CMD_READ      				(0<<7)
#define CMD_WRITE     				(1<<7)

// define register
#define REG_CONFIG    				0x00
#define REG_RTD_MSB   				0x01
#define REG_RTD_LSB    				0x02
#define REG_HIGH_FAULT_MSB  		0x03
#define REG_HIGH_FAULT_LSB  		0x04
#define REG_LOW_FAULT_MSB   		0x05
#define REG_LOW_FAULT_LSB   		0x06
#define REG_FAULT_STATUS    		0x07

#define CONFIG_VBIAS_ON 			(1<<7)
#define CONFIG_VBIAS_OFF 			(0<<7)
#define CONFIG_MODE_AUTO 			(1<<6)
#define CONFIG_MODE_OFF 			(0<<6)
#define CONFIG_1SHOT 				(1<<5)
#define CONFIG_3WIRE 				(1<<4)
#define CONFIG_24WIRE 				(0<<4)
#define CONFIG_FAULTCYCLE_NO 		(0<<2)
#define CONFIG_FAULTCYCLE_AUTO 		(1<<2)
#define CONFIG_FAULTCYCLE_MANUAL1 	(2<<2)
#define CONFIG_FAULTCYCLE_MANUAL2 	(3<<2)
#define CONFIG_FAULTSTATCLEAR 		(1<<1)
#define CONFIG_FILT50HZ 			(1<<0)
#define CONFIG_FILT60HZ 			(0<<0)

#define CONFIG	(U8)(CONFIG_VBIAS_ON | CONFIG_MODE_AUTO | CONFIG_FAULTCYCLE_NO | CONFIG_24WIRE | CONFIG_FILT60HZ) // enable Vbias; autoconvert on; 4-wire; 60Hz;

#define N_CHIPS 8


//=================================================================================================
// Section:       MACROS
// Description:   Definition of local macros (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of local enumerations (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of local Structures (visible by this module only).
//=================================================================================================


//=================================================================================================
// Section:       LOCAL VARIABLES
// Description:   Definition of local variables (visible by this module only).
//=================================================================================================
      
U16 au16CS_Pins[] = {CS0_Pin, CS1_Pin, CS2_Pin, CS3_Pin, CS4_Pin, CS5_Pin, CS6_Pin, CS7_Pin};
GPIO_TypeDef * astCS_Ports[] = {CS0_GPIO_Port, CS1_GPIO_Port, CS2_GPIO_Port, CS3_GPIO_Port, CS4_GPIO_Port, CS5_GPIO_Port, CS6_GPIO_Port, CS7_GPIO_Port};

SPI_HandleTypeDef * hspi = 0;

BOOL boDataReady = TRUE;

PRIVATE S32 as32Temp[N_CHIPS] = {};

//=================================================================================================
// Section:       LOCAL CONSTANTS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       LOCAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of local functions (visible by this module only).
//=================================================================================================

PRIVATE BOOL boWriteReg( U8 u8Chipselect, U8 u8Register, PU16 pu16Data, BOOL boIs16bit );
PRIVATE BOOL boReadReg( U8 u8Chipselect, U8 u8Register, PU16 pu16Data, BOOL boIs16bit );

PRIVATE S32 s32ConvertTemp( U16 u16RTD );
PRIVATE BOOL boReadTemp( U8 u8Chipselect, PS32 ps32Temp );
PRIVATE BOOL boReadAllTemp( PU32 pas32Temp );

PRIVATE BOOL boWriteConfig(U8 u8CS, U16 u16Config);

//=================================================================================================
// Section:       EXTERNAL FUNCTIONS
// Description:   Definition of external (global) functions.
//=================================================================================================



//=================================================================================================
// Section:       EXTERNAL VARIABLES
// Description:   Definition of external (global) variables.
//=================================================================================================


//=================================================================================================
// Section:       GLOBAL FUNCTIONS
// Description:   Definition (implementation) of global functions.
//=================================================================================================



//-------------------------------------------------------------------------------------------------
// Function:      RTDC_Init
// Description:   Init MAX chips
// Parameters:    SPI_HandleTypeDef * hspi_init
// Returns:       Boolean TRUE if successful transmit
//-------------------------------------------------------------------------------------------------
BOOL RTDC_Init(SPI_HandleTypeDef * hspi_init){

	BOOL boOK = TRUE;

	hspi = hspi_init;

	for(U8 u8CS = 0; u8CS < N_CHIPS; u8CS ++){
		HAL_GPIO_WritePin(astCS_Ports[u8CS], au16CS_Pins[u8CS], GPIO_PIN_SET);
	}

	U16 u16Config = CONFIG | CONFIG_FAULTSTATCLEAR;

	for(U8 u8CS = 0; u8CS < N_CHIPS; u8CS ++){
		boOK &= boWriteReg( u8CS, REG_CONFIG, &u16Config, FALSE );
	}

	return boOK;
}


//-------------------------------------------------------------------------------------------------
// Function:      RTDC_Init
// Description:   Init MAX chips
// Parameters:    SPI_HandleTypeDef * hspi_init
// Returns:       Boolean TRUE if successful transmit
//-------------------------------------------------------------------------------------------------
VOID RTDC_Loop(VOID){

	if( boDataReady ){

		boReadAllTemp(as32Temp);

	}

}

//-------------------------------------------------------------------------------------------------
// Function:      vGetTempArray
// Description:   Init MAX chips
// Parameters:    SPI_HandleTypeDef * hspi_init
// Returns:       Boolean TRUE if successful transmit
//-------------------------------------------------------------------------------------------------
VOID vGetTempArray(PS32 pas32TempArray){

	pas32TempArray = &as32Temp;

}





//=================================================================================================
// Section:       LOCAL FUNCTIONS
// Descriptionn:  Definition (implementation) of local functions.
//=================================================================================================

PRIVATE BOOL boWriteConfig(U8 u8CS, U16 u16Config){
	BOOL boOK = TRUE;

	boOK &= boWriteReg( u8CS, REG_CONFIG, &u16Config, FALSE );

	return boOK;
}

//-------------------------------------------------------------------------------------------------
// Function:      boWriteReg
// Description:   write to register from 1 chip, can be 16bit or 8 bit
// Parameters:    U8 u8Chipselect (0-7)
// 				  U8 u8Register (Register from chip)
//				  U16 u16Data
//				  BOOL boIs16bit (TRUE, if Data is 16bit, FALSE, if Data is 8bit)
// Returns:       Boolean TRUE if successful transmit
//-------------------------------------------------------------------------------------------------
PRIVATE BOOL boWriteReg( U8 u8Chipselect, U8 u8Register, PU16 pu16Data, BOOL boIs16bit )
{
    BOOL boOK = TRUE;

    if( hspi == 0 ) return FALSE;

    u8Register |= CMD_WRITE;

    HAL_GPIO_WritePin(astCS_Ports[u8Chipselect], au16CS_Pins[u8Chipselect], GPIO_PIN_RESET);

    boOK &= HAL_SPI_Transmit(hspi, &u8Register, 1, 100) == HAL_OK ? TRUE : FALSE;

    if( boIs16bit ){
    	*pu16Data = UTIL_u16RevU16(*pu16Data);
    	boOK &= HAL_SPI_Transmit(hspi, (PU8)pu16Data, 2, 100) == HAL_OK ? TRUE : FALSE;
    } else {
    	boOK &= HAL_SPI_Transmit(hspi, (PU8)pu16Data, 1, 100) == HAL_OK ? TRUE : FALSE;
    }

    HAL_GPIO_WritePin(astCS_Ports[u8Chipselect], au16CS_Pins[u8Chipselect], GPIO_PIN_SET);

    return boOK;
}

//-------------------------------------------------------------------------------------------------
// Function:      boReadReg
// Description:   reads from register from 1 chip, can be 16bit or 8 bit
// Parameters:    U8 u8Chipselect (0-7)
// 				  U8 u8Register (Register from chip)
//				  PU16 pu16Data
//				  BOOL boIs16bit (TRUE, if Data is 16bit, FALSE, if Data is 8bit)
// Returns:       Boolean TRUE if successful receive
//-------------------------------------------------------------------------------------------------
PRIVATE BOOL boReadReg( U8 u8Chipselect, U8 u8Register, PU16 pu16Data, BOOL boIs16bit )
{
    BOOL boOK = TRUE;

    if( hspi == 0 ) return FALSE;

    u8Register |= CMD_READ;

    HAL_GPIO_WritePin(astCS_Ports[u8Chipselect], au16CS_Pins[u8Chipselect], GPIO_PIN_RESET);

    boOK &= HAL_SPI_Transmit(hspi, &u8Register, 1, 100) == HAL_OK ? TRUE : FALSE;

    if( boIs16bit ){
    	boOK &= HAL_SPI_Receive(hspi, (PU8)pu16Data, 2, 100) == HAL_OK ? TRUE : FALSE;
    	*pu16Data = UTIL_u16RevU16(*pu16Data);
    } else {
    	boOK &= HAL_SPI_Receive(hspi, (PU8)pu16Data, 1, 100) == HAL_OK ? TRUE : FALSE;
    }

    HAL_GPIO_WritePin(astCS_Ports[u8Chipselect], au16CS_Pins[u8Chipselect], GPIO_PIN_SET);

    return boOK;
}

//-------------------------------------------------------------------------------------------------
// Function:      boReadRTD
// Description:   reads RTD from chip
// Parameters:    U8 u8Chipselect (0-7)
//				  PU16 pu16Data
// Returns:       Boolean TRUE if successful receive
//-------------------------------------------------------------------------------------------------
PRIVATE BOOL boReadTemp( U8 u8Chipselect, PS32 ps32Temp )
{
    BOOL boOK = TRUE;
    U16 u16RTD = 0;

    boOK &= boReadReg( u8Chipselect, REG_RTD_MSB, &u16RTD, TRUE );

    if(u16RTD & 0x0001){

    	U16 u16Error = 0;

    	boOK &= boReadReg( u8Chipselect, REG_FAULT_STATUS, &u16Error, FALSE );


    	boWriteConfig(u8Chipselect, CONFIG | CONFIG_FAULTSTATCLEAR);

    	/** @todo error handling */
    	return FALSE;
    }

    //u16RTD = u16RTD >> 1;
    u16RTD = u16RTD & 0xFFFE;

    *ps32Temp = s32ConvertTemp( u16RTD );

    return boOK;
}

//-------------------------------------------------------------------------------------------------
// Function:      boReadRTD
// Description:   reads RTD from chip
// Parameters:    U8 u8Chipselect (0-7)
//				  PU16 pu16Data
// Returns:       Boolean TRUE if successful receive
//-------------------------------------------------------------------------------------------------
PRIVATE BOOL boReadAllTemp( PU32 pas32Temp )
{
    BOOL boOK = TRUE;


    for( int i = 0; i < N_CHIPS; i++ ){
    	boOK &= boReadTemp(i, pas32Temp + i);
    }

    return boOK;
}

//-------------------------------------------------------------------------------------------------
// Function:      flConvertADCData
// Description:   Converts resistor value to temperature data
// Parameters:    U16 u16RTemp
// Returns:       U16, temperature in Celcius
//-------------------------------------------------------------------------------------------------
PRIVATE S32 s32ConvertTemp( U16 u16RTD )
{

	S32 z = u16RTD - 8192;
	S32 y = (((z*40843)>>16) + 130994)*z;
	y = ((y>>6)*25)>>14;

	return y;
}








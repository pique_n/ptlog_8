//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       STM32L4EvalBoard
// Author:        Melvin Deubelbeiss  (melvin.deubelbeiss@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        Display Driver
// Filename:      DISD_DisplayDriver.h
// Date:          12.12.2022
// Revision:      1.0
//
//-------------------------------------------------------------------------------------------------
//=================================================================================================

#ifndef INC_DISD_DISPLAYDRIVER_H_
#define INC_DISD_DISPLAYDRIVER_H_

#ifdef __cplusplus
extern "C" {
#endif

//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files (visible by all modules).
//=================================================================================================

#include "SDEF_StandardDefinitions.h"
#include "main.h"
#include "string.h"

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================

//=================================================================================================
// Section:       MACROS
// Description:   Definition of global macros (visible by all modules).
//=================================================================================================

//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of global enumerations (visible by all modules).
//=================================================================================================

typedef enum {
	eOrNormal = 0,
	eOrCW = 1,
	eOrCCW = 2,
	eOrFlipped = 3
} DISD_enOrientation;

typedef enum {
	eCommand = 0,
	eData = 1,
} DISD_eCMDPin;

//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of global Structures (visible by all modules).
//=================================================================================================

typedef struct {
	U8 u8Red;
	U8 u8Green;
	U8 u8Blue;
} DISD_stColor;

typedef struct {
	SPI_HandleTypeDef* hspi;
	U32 u32SPITimeout;
	GPIO_TypeDef* CS_GPIOx;
	U16 u16CS_Pin;
	GPIO_TypeDef* RST_GPIOx;
	U16 u16RST_Pin;
	GPIO_TypeDef* A0_GPIOx;
	U16 u16A0_Pin;
} DISD_LLDefinition;

typedef struct {
	CONST DISD_LLDefinition* pstLLDF;
	U8 u8ScreenWidth;
	U8 u8ScreenHeight;
	U8 u8ScreenOffsetWidth;
	U8 u8ScreenOffsetHeight;
} DISD_HardwareDefinition;

typedef struct {
	DISD_HardwareDefinition* pstHWDF;
	CONST U8 u8Count;
} DISD_ObjectDefinition;

//=================================================================================================
// Section:       GLOBAL VARIABLES
// Description:   Definition of global variables (visible by all modules).
//=================================================================================================

//=================================================================================================
// Section:       GLOBAL CONSTANTS
// Description:   Definition of global constants (visible by all modules).
//=================================================================================================

// Defined in DISD_Font_XxX.c files
extern CONST CHAR chFont5x7[480];
extern CONST CHAR chFont5x8[1280];
extern CONST CHAR chFont6x8[128][8];
extern CONST CHAR chFont8x8[128][8];

//=================================================================================================
// Section:       GLOBAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of global functions (visible by all modules).
//=================================================================================================

VOID DISD_vSetObject(CONST DISD_ObjectDefinition* pstObjectDefinition);

// Default hardware definition set/get
VOID DISD_vSetDefaultHWDF(U8 u8DefaultDefinition);

// Functions with hardware definition as parameter
VOID DISD_vHWDFInit(U8 u8HWDF);
VOID DISD_vHWDFClear(U8 u8HWDF, DISD_stColor* pstColor);
VOID DISD_vHWDFWriteString(U8 u8HWDF, PCHAR chString, U16 u16Len, U8 u8X, U8 u8Y, DISD_stColor* pstFrontColor, DISD_stColor* pstBackColor);
VOID DISD_vHWDFSetOrientation(U8 u8HWDF, DISD_enOrientation enOrientation);
VOID DISD_vHWDFDrawLine(U8 u8HWDF, U8 u8X0, U8 u8Y0, U8 u8X1, U8 u8Y1, DISD_stColor* pstColor);

// Functions using default hardware definition
VOID DISD_vInit();
VOID DISD_vClear(DISD_stColor* pstColor);
VOID DISD_vWriteString(PCHAR chString, U16 u16Len, U8 u8X, U8 u8Y, DISD_stColor* pstFrontColor, DISD_stColor* pstBackColor);
VOID DISD_vSetOrientation(DISD_enOrientation enOrientation);

#ifdef __cplusplus
}
#endif

#endif /* INC_DISD_DISPLAYDRIVER_H_ */

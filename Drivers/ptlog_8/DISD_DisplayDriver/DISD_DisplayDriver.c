//=================================================================================================
//
// Company:       Paul Scherrer Institut
//                5232 Villigen PSI
//                Switzerland
//
//-------------------------------------------------------------------------------------------------
//
// Project:       STM32L4EvalBoard
// Author:        Melvin Deubelbeiss (melvin.deubelbeiss@psi.ch)
//
//-------------------------------------------------------------------------------------------------
//
// Module:        Display Driver
// Filename:      DISD_DisplayDriver.c
// Date:          12.12.2022
// Revision:      1.0
//
//-------------------------------------------------------------------------------------------------
//=================================================================================================

//=================================================================================================
// Section:       INCLUDES
// Description:   List of required include files.
//=================================================================================================

#include "DISD_DisplayDriver/DISD_DisplayDriver.h"

//=================================================================================================
// Section:       DEFINITIONS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       MACROS
// Description:   Definition of local macros (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       ENUMERATIONS
// Description:   Definition of local enumerations (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       STRUCTURES
// Description:   Definition of local Structures (visible by this module only).
//=================================================================================================

//=================================================================================================
// Section:       LOCAL FUNCTIONS (PROTOTYPES)
// Description:   Definition of local functions (visible by this module only).
//=================================================================================================

PRIVATE VOID vWriteCMD(CONST DISD_LLDefinition* stLLDR, U8 u8Value);
PRIVATE VOID vWriteData(CONST DISD_LLDefinition* stLLDR, U8 u8Value);
PRIVATE VOID vSetAddr(CONST DISD_LLDefinition* stLLDR, U16 u16XStart, U16 u16YStart, U16 u16XEnd, U16 u16YEnd);
PRIVATE VOID vWriteChar(CONST DISD_LLDefinition* stLLDR, CHAR chChar, U8 u8X, U8 u8Y, DISD_stColor* pstFrontColor, DISD_stColor* pstBackColor);
PRIVATE U32 u32GetColor(DISD_stColor* pstColor);

//=================================================================================================
// Section:       LOCAL VARIABLES
// Description:   Definition of local variables (visible by this module only).
//=================================================================================================

LOCAL DISD_ObjectDefinition* pstObject = NULL;
LOCAL U8 u8DefaultHWDF = 0;

//=================================================================================================
// Section:       LOCAL CONSTANTS
// Description:   Definition of local constants (visible by this module only).
//=================================================================================================

CONST U8 ADR_RANGE_W = 128;
CONST U8 ADR_RANGE_H = 160;
CONST U8 SCR_W = 160;
CONST U8 SCR_H = 128;
CONST U8 OFFSET_W = 1;
CONST U8 OFFSET_H = 1;

//=================================================================================================
// Section:       EXTERNAL FUNCTIONS
// Description:   Definition of external (global) functions.
//=================================================================================================

//=================================================================================================
// Section:       EXTERNAL VARIABLES
// Description:   Definition of external (global) variables.
//=================================================================================================

//=================================================================================================
// Section:       GLOBAL FUNCTIONS
// Description:   Definition (implementation) of global functions.
//=================================================================================================

VOID DISD_vSetObject(CONST DISD_ObjectDefinition* pstObjectDefinition){
	pstObject = (DISD_ObjectDefinition*) pstObjectDefinition;
}

VOID DISD_vSetDefaultHWDF(U8 u8DefaultDefinition){
	if(u8DefaultDefinition > (pstObject->u8Count) - 1){
		return;
	}
	u8DefaultHWDF = u8DefaultDefinition;
}

VOID DISD_vHWDFInit(U8 u8HWDF){
	if(u8HWDF > (pstObject->u8Count - 1)){
		return;
	}

	// Hardware reset
	HAL_GPIO_WritePin(pstObject->pstHWDF[u8HWDF].pstLLDF->CS_GPIOx, pstObject->pstHWDF[u8HWDF].pstLLDF->u16CS_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(pstObject->pstHWDF[u8HWDF].pstLLDF->RST_GPIOx, pstObject->pstHWDF[u8HWDF].pstLLDF->u16RST_Pin, GPIO_PIN_SET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(pstObject->pstHWDF[u8HWDF].pstLLDF->RST_GPIOx, pstObject->pstHWDF[u8HWDF].pstLLDF->u16RST_Pin, GPIO_PIN_RESET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(pstObject->pstHWDF[u8HWDF].pstLLDF->RST_GPIOx, pstObject->pstHWDF[u8HWDF].pstLLDF->u16RST_Pin, GPIO_PIN_SET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(pstObject->pstHWDF[u8HWDF].pstLLDF->CS_GPIOx, pstObject->pstHWDF[u8HWDF].pstLLDF->u16CS_Pin, GPIO_PIN_RESET);

	vWriteCMD(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x11);
	HAL_Delay(200);

	vWriteCMD(pstObject->pstHWDF[u8HWDF].pstLLDF, 0xb1);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x05);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x3c);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x3c);

	vWriteCMD(pstObject->pstHWDF[u8HWDF].pstLLDF, 0xb2);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x05);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x3c);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x3c);

	vWriteCMD(pstObject->pstHWDF[u8HWDF].pstLLDF, 0xb3);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x05);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x3c);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x3c);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x05);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x3c);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x3c);

	vWriteCMD(pstObject->pstHWDF[u8HWDF].pstLLDF, 0xb4);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x03);

	vWriteCMD(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x3a);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x05);

	vWriteCMD(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x20);

	vWriteCMD(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x13);

	vWriteCMD(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x26);
	vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x04);

	vWriteCMD(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x29);

	HAL_GPIO_WritePin(pstObject->pstHWDF[u8HWDF].pstLLDF->CS_GPIOx, pstObject->pstHWDF[u8HWDF].pstLLDF->u16CS_Pin, GPIO_PIN_SET);

	DISD_vHWDFSetOrientation(u8HWDF, eOrNormal);
}

VOID DISD_vHWDFClear(U8 u8HWDF, DISD_stColor* pstColor){
	if(u8HWDF > (pstObject->u8Count - 1)){
		return;
	}

	U16 u16Color = u32GetColor(pstColor);

	HAL_GPIO_WritePin(pstObject->pstHWDF[u8HWDF].pstLLDF->CS_GPIOx, pstObject->pstHWDF[u8HWDF].pstLLDF->u16CS_Pin, GPIO_PIN_RESET);

	vSetAddr(pstObject->pstHWDF[u8HWDF].pstLLDF, 0, 0, (pstObject->pstHWDF[u8HWDF].u8ScreenWidth + pstObject->pstHWDF[u8HWDF].u8ScreenOffsetWidth), (pstObject->pstHWDF[u8HWDF].u8ScreenHeight + pstObject->pstHWDF[u8HWDF].u8ScreenOffsetHeight));

	U16 u16Cnt;
	for(u16Cnt = 0; u16Cnt < ADR_RANGE_H * ADR_RANGE_W; u16Cnt++){
		vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, (U8) ((u16Color >> 8) & 0xFF));
		vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, (U8) (u16Color));
	}

	HAL_GPIO_WritePin(pstObject->pstHWDF[u8HWDF].pstLLDF->CS_GPIOx, pstObject->pstHWDF[u8HWDF].pstLLDF->u16CS_Pin, GPIO_PIN_SET);
}

VOID DISD_vHWDFWriteString(U8 u8HWDF, PCHAR chString, U16 u16Len, U8 u8X, U8 u8Y, DISD_stColor* pstFrontColor, DISD_stColor* pstBackColor){
	if(u8HWDF > (pstObject->u8Count - 1)){
		return;
	}

	u8X += pstObject->pstHWDF[u8HWDF].u8ScreenOffsetWidth;
	u8Y += pstObject->pstHWDF[u8HWDF].u8ScreenOffsetHeight;

	U16 u16Cnt;
	for(u16Cnt = 0; u16Cnt < u16Len; u16Cnt++){
		vWriteChar(pstObject->pstHWDF[u8HWDF].pstLLDF, chString[u16Cnt], u8X, u8Y, pstFrontColor, pstBackColor);

		if(u8X < ((pstObject->pstHWDF[u8HWDF].u8ScreenWidth + pstObject->pstHWDF[u8HWDF].u8ScreenOffsetWidth) - 6)){
			u8X += 6;
		}
		else if(u8Y < ((pstObject->pstHWDF[u8HWDF].u8ScreenHeight + pstObject->pstHWDF[u8HWDF].u8ScreenOffsetHeight) - 8)){
			u8X = pstObject->pstHWDF[u8HWDF].u8ScreenOffsetWidth;
			u8Y += 8;
		}
		else{
			u8X = pstObject->pstHWDF[u8HWDF].u8ScreenOffsetWidth;
			u8Y = pstObject->pstHWDF[u8HWDF].u8ScreenOffsetHeight;
		}
	}
}

VOID DISD_vHWDFSetOrientation(U8 u8HWDF, DISD_enOrientation enOrientation){
	if(u8HWDF > (pstObject->u8Count - 1)){
		return;
	}

	HAL_GPIO_WritePin(pstObject->pstHWDF[u8HWDF].pstLLDF->CS_GPIOx, pstObject->pstHWDF[u8HWDF].pstLLDF->u16CS_Pin, GPIO_PIN_RESET);

	vWriteCMD(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x36);    // Memory data access control

	switch(enOrientation){
		case eOrCW:
			pstObject->pstHWDF[u8HWDF].u8ScreenWidth = SCR_H;
			pstObject->pstHWDF[u8HWDF].u8ScreenHeight = SCR_W;
			pstObject->pstHWDF[u8HWDF].u8ScreenOffsetWidth = OFFSET_H;
			pstObject->pstHWDF[u8HWDF].u8ScreenOffsetHeight = OFFSET_W;
			vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0xc8);
			break;
		case eOrCCW:
			pstObject->pstHWDF[u8HWDF].u8ScreenWidth = SCR_H;
			pstObject->pstHWDF[u8HWDF].u8ScreenHeight = SCR_W;
			pstObject->pstHWDF[u8HWDF].u8ScreenOffsetWidth = OFFSET_H;
			pstObject->pstHWDF[u8HWDF].u8ScreenOffsetHeight = OFFSET_W;
			vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x08);
			break;
		case eOrFlipped:
			pstObject->pstHWDF[u8HWDF].u8ScreenWidth = SCR_W;
			pstObject->pstHWDF[u8HWDF].u8ScreenHeight = SCR_H;
			pstObject->pstHWDF[u8HWDF].u8ScreenOffsetWidth = OFFSET_W;
			pstObject->pstHWDF[u8HWDF].u8ScreenOffsetHeight = OFFSET_H;
			vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0xa8);
		case eOrNormal:
		default:
			pstObject->pstHWDF[u8HWDF].u8ScreenWidth = SCR_W;
			pstObject->pstHWDF[u8HWDF].u8ScreenHeight = SCR_H;
			pstObject->pstHWDF[u8HWDF].u8ScreenOffsetWidth = OFFSET_W;
			pstObject->pstHWDF[u8HWDF].u8ScreenOffsetHeight = OFFSET_H;
			vWriteData(pstObject->pstHWDF[u8HWDF].pstLLDF, 0x68);
			break;
	}

	HAL_GPIO_WritePin(pstObject->pstHWDF[u8HWDF].pstLLDF->CS_GPIOx, pstObject->pstHWDF[u8HWDF].pstLLDF->u16CS_Pin, GPIO_PIN_SET);
}

//VOID DISD_vHWDFDrawLine(U8 u8HWDF, U8 u8X0, U8 u8Y0, U8 u8X1, U8 u8Y1, DISD_stColor* pstColor){
//	if(u8HWDF > (pstObject->u8Count - 1)){
//		return;
//	}
//
//	S8 s8DeltaX = u8X1 - u8X0;
//	S8 sDeltaY = u8Y1 - u8Y0;
//
//	FLOAT flSteigung = (FLOAT) (s8DeltaY) / (FLOAT) (s8DeltaX);
//
//	U8 u8Len;
//	if(s8DeltaX < 0){
//		u8Len = (U8) (s8DeltaX * -1);
//	}
//
//	U8 u8Cnt;
//	for(u8Cnt = 0; u8Cnt < u8Len; u8Cnt++){
//		U8 u8YVal = (U8)(flSteigung * u8Cnt) + u8Y0;
//		U8 u8XVal = u8Cnt + u8X0;
//	}
//}

VOID DISD_vInit(){
	if(pstObject == NULL){
		return;
	}

	DISD_vHWDFInit(u8DefaultHWDF);
}

VOID DISD_vClear(DISD_stColor* pstColor){
	if(pstObject == NULL){
		return;
	}

	DISD_vHWDFClear(u8DefaultHWDF, pstColor);
}

VOID DISD_vWriteString(PCHAR chString, U16 u16Len, U8 u8X, U8 u8Y, DISD_stColor* pstFrontColor, DISD_stColor* pstBackColor){
	if(pstObject == NULL){
		return;
	}

	DISD_vHWDFWriteString(u8DefaultHWDF, chString, u16Len, u8X, u8Y, pstFrontColor, pstBackColor);
}

VOID DISD_vSetOrientation(DISD_enOrientation enOrientation){
	if(pstObject == NULL){
		return;
	}

	DISD_vHWDFSetOrientation(u8DefaultHWDF, enOrientation);
}

//=================================================================================================
// Section:       LOCAL FUNCTIONS
// Description:  Definition (implementation) of local functions.
//=================================================================================================

VOID vWriteCMD(CONST DISD_LLDefinition* stLLDR, U8 u8Value){
	HAL_GPIO_WritePin(stLLDR->A0_GPIOx, stLLDR->u16A0_Pin, eCommand);
	HAL_SPI_Transmit(stLLDR->hspi, &u8Value, 1, stLLDR->u32SPITimeout);
}

VOID vWriteData(CONST DISD_LLDefinition* stLLDR, U8 u8Value){
	HAL_GPIO_WritePin(stLLDR->A0_GPIOx, stLLDR->u16A0_Pin, eData);
	HAL_SPI_Transmit(stLLDR->hspi, &u8Value, 1, stLLDR->u32SPITimeout);
}

VOID vSetAddr(CONST DISD_LLDefinition* stLLDR, U16 u16XStart, U16 u16YStart, U16 u16XEnd, U16 u16YEnd){
	vWriteCMD(stLLDR, 0x2a);    // Column address
	vWriteData(stLLDR, (u16XStart >> 8) & 0xFF);
	vWriteData(stLLDR, u16XStart & 0xFF);
	vWriteData(stLLDR, (u16XEnd >> 8) & 0xFF);
	vWriteData(stLLDR, u16XEnd & 0xFF);

	vWriteCMD(stLLDR, 0x2b);
	vWriteData(stLLDR, (u16YStart >> 8) & 0xFF);
	vWriteData(stLLDR, u16YStart & 0xFF);
	vWriteData(stLLDR, (u16YEnd >> 8) & 0xFF);
	vWriteData(stLLDR, u16YEnd & 0xFF);

	vWriteCMD(stLLDR, 0x2c);
}

U32 u32GetColor(DISD_stColor* pstColor){
	return ((pstColor->u8Blue >> 3) << 11) | ((pstColor->u8Green >> 2) << 5) | (pstColor->u8Red >> 3);
}

VOID vWriteChar(CONST DISD_LLDefinition* stLLDR, CHAR chChar, U8 u8X, U8 u8Y, DISD_stColor* pstFrontColor, DISD_stColor* pstBackColor){
	U8 u8Buffer[5];

	U16 u16FrontColor = u32GetColor(pstFrontColor);
	U16 u16BackColor = u32GetColor(pstBackColor);

	memcpy(u8Buffer, &chFont5x7[(chChar - 32) * 5], 5);

	HAL_GPIO_WritePin(stLLDR->CS_GPIOx, stLLDR->u16CS_Pin, GPIO_PIN_RESET);
	vSetAddr(stLLDR, u8X, u8Y, u8X + 4, u8Y + 6);

	U8 u8CntZeilen;
	U8 u8CntSpalten;
	for(u8CntZeilen = 0; u8CntZeilen < 7; u8CntZeilen++){
		for(u8CntSpalten = 0; u8CntSpalten < 5; u8CntSpalten++){
			if((u8Buffer[u8CntSpalten] >> u8CntZeilen) & 0x01){
				vWriteData(stLLDR, (u16FrontColor >> 8) & 0xFF);
				vWriteData(stLLDR, u16FrontColor & 0xFF);
			}
			else{
				vWriteData(stLLDR, (u16BackColor >> 8) & 0xFF);
				vWriteData(stLLDR, u16BackColor & 0xFF);
			}
		}
	}

	HAL_GPIO_WritePin(stLLDR->CS_GPIOx, stLLDR->u16CS_Pin, GPIO_PIN_SET);
}

